class Data_base:
    students__ = [['Goloveshkin Sergey ', '1', '15567-DH'], ['Gorodetskaya Yana Filippovna', '1', '15567-DH'],['Fedoseeva Irina Kosparovna', '1', '15567-DH'],['Polubeshev Evgenii Ignatievich', '1', '15567-DH'],['Vasiliev Petr Ignatievich', '1', '15567-DH'],['Ibragimov Sergey Eduardovich', '2', '17767-DH'],['Omarova Veronika Genadievna', '2', '17767-DH'],['Karamzin Nikolay Evgenievich', '2', '17767-DH'],['Uvalov Nikolay Timurovich', '2', '17767-DH']]
    login_passwords = [['Goloveshkin', '0000'],['Gorodetskaya', '0000'],['Fedoseeva', '0000'], ['Polubeshev', '0000'], ['Polyak','0000'], ['Katamaranov', '0000'],['Lobysheva', '0000'],['Marinovskaya', '0000'],['Ibragimov', '0000'],['Omarova', '0000'],['Karamzin', '0000'],['Uvalov', '0000']]
    teachers = [['Polyak Anjella Viktorovna', 'Математика'],['Katamaranov Ignat Khristoforovich','История'],['Lobysheva Inga Vasilievna','Русский язык'], ['Marinovskaya Olga Genadievna','Программирование']]
    status_ = (('Goloveshkin','студент'),('Gorodetskaya','студент'),('Fedoseeva','студент'),('Polubeshev','студент'),('Polyak','преподаватель'),('Lobysheva','преподаватель'),('Katamaranov','преподаватель'),('Marinovskaya','преподаватель'),('Ibragimov','студент'),('Omarova','студент'),('Karamzin','студент'),('Uvalov','студент'))
    history_list = [('Аверченко, А.К. История органов следствия России. Хрестоматия. / А.К. Аверченко и др. - М.: ЮНИТИ, 2015. - 223 c.'),('Андреев, И.Л. История России с древнейших времен до 1861 года: Учебник для бакалавров / Н.И. Павленко, И.Л. Андреев, В.А. Федоров . - М.: Юрайт, ИД Юрайт, 2012. - 712 c.'),('Андреевский, И.Е. О наместниках, воеводах и губернаторах: История государственного управления в России (IX-XIX вв.) / И.Е. Андреевский. - М.: КД Либроком, 2014. - 166 c.'),('Анисимов, Е. История России от Рюрика до Путина. Люди, события, даты / Е. Анисимов. - СПб.: Питер, 2013. - 592 c.'), ('Анисимов, Е.В. История России от Рюрика до Путина. Люди. События. Даты / Е.В. Анисимов. - СПб.: Питер, 2013. - 592 c.'),('Антонова, Л.В. Тайная история спецслужб России / Л.В. Антонова. - Рн/Д: Феникс, 2012. - 255 c.')]
    math_list = [('Баврин, И. И. Математика для технических колледжей и техникумов : учебник и практикум для среднего профессионального образования / И. И. Баврин. — 2-е изд., испр. и доп. — Москва : Издательство Юрайт, 2019. — 397 с.'),('Баврин, И. И. Математика : учебник и практикум для среднего профессионального образования / И. И. Баврин. — 2-е изд., перераб. и доп. — Москва : Издательство Юрайт, 2019. — 616 с. '), ('Баврин, И. И. Математика : учебник и практикум для среднего профессионального образования / И. И. Баврин. — 2-е изд., перераб. и доп. — Москва : Издательство Юрайт, 2019. — 616 с. '),('Богомолов, Н. В. Математика. Задачи с решениями в 2 ч. Часть 1 : учебное пособие для среднего профессионального образования / Н. В. Богомолов. — 2-е изд., испр. и доп. — Москва : Издательство Юрайт, 2019. — 439 с.'), ('Богомолов, Н. В. Математика : учебник для прикладного бакалавриата / Н. В. Богомолов, П. И. Самойленко. — 5-е изд., перераб. и доп. — Москва : Издательство Юрайт, 2019. — 401 с.')]
    programming_list = [('Александреску, А. Язык программирования D / А. Александреску. — М.: Символ, 2013. — 536 c.'),('Ашарина, И.В. Основы программирования на языках С и С++: Курс лекций для высших учебных заведений / И.В. Ашарина. — М.: Гор. линия-Телеком, 2012. — 208 c.'),('Баженова, И.Ю. Языки программирования: Учебник для студентов учреждений высш. проф. образования / И.Ю. Баженова; Под ред. В.А. Сухомлин. — М.: ИЦ Академия, 2012. — 368 c.')]
    academic_performance_math_15567_DH = {'Goloveshkin': 17, 'Gorodetskaya' : 44, 'Fedoseeva' : 25, 'Polubeshev' : 12}
    academic_performance_math_17767_DH = {'Ibragimov' : 14, 'Omarova': 0, 'Karamzin' : 32, 'Uvalov' : 58}
    academic_performance_history_15567_DH = {'Goloveshkin': 19, 'Gorodetskaya' : 25, 'Fedoseeva' : 27, 'Polubeshev' : 17}
    academic_performance_history_17767_DH = {'Ibragimov' : 14, 'Omarova': 0, 'Karamzin' : 56, 'Uvalov' : 58}
    academic_performance_programming_15567_DH = {'Goloveshkin': 13, 'Gorodetskaya' : 44, 'Fedoseeva' : 27, 'Polubeshev' : 48}
    academic_performance_programming_17767_DH = {'Ibragimov' : 14, 'Omarova':37, 'Karamzin' : 56, 'Uvalov' : 58}
    academic_performance_rus_15567_DH = {'Goloveshkin': 65, 'Gorodetskaya' : 15, 'Fedoseeva' : 27, 'Polubeshev' : 86}
    academic_performance_rus_17767_DH = {'Ibragimov' : 14, 'Omarova': 0, 'Karamzin' : 65, 'Uvalov' : 58}
    timetable_15567_DH = [('Пн','Математика 11:50', 'Программирование 13:10', 'Русский язык 14:40'), ('Вт','Математика 11:50','Русский язык 13:10'), ('Ср','История 11:50','История 14:40'), ('Чт','Программирование 13:10','История 14:40'), ('Пт','Русский язык 11:50','Математика 14:40'), ('Сб','Программирование 14:40') ]
    timetable_17767_DH = [('Пн','Математика 14:40', 'Программирование 15:30', 'Русский язык 17:10'), ('Вт','Русский язык 11:50','Математика 14:40'), ('Ср','История 11:50','История 14:40'), ('Чт','Программирование 14:40','История 15:30'), ('Пт','Математика 14:40'), ('Сб','Программирование 14:40') ]
    information_about_university = ['''Иркутский государственный университет учрежден 27 октября 1918 года. Его открытие положило начало высшему образованию в Сибири и на Дальнем Востоке. Университет сразу стал главным образовательным, научным и культурным центром на огромной территории от Енисея до Тихого океана.

Сегодня Иркутский государственный университет является крупнейшим в регионе научным и образовательным учреждением естественно-научного и гуманитарного профилей. Обучение в университете ведется по всему спектру естественно-научных, математических, гуманитарных, общественных, педагогических, лингвистических дисциплин, а также частично инженерных. Образовательный комплекс вуза включает 8 учебных институтов, 8 факультетов, 1 филиал, одну из крупнейших вузовских библиотек России, магистратуру, аспирантуру, докторантуру и Иркутский виртуальный университет, являющийся базовым в регионе.

В настоящее время в структуре университета 10 крупных научных подразделений: научно-исследовательская часть (НИЧ), 3 научно-исследовательских института (прикладной физики, биологии, нефте- и углехимического синтеза), Центр новых информационных технологий (ЦНИТ), НОЦ «Байкал», Межрегиональный институт общественных наук (МИОН), научно-исследовательский центр «Байкальский регион», Астрономическая обсерватория, Ботанический сад.

Численность профессорско-преподавательского состава – 1155 человек, в том числе 142 докторов наук, профессоров, 625 кандидатов наук, 1 академик РАН, 1 член-корреспондент РАН, 3 заслуженных деятеля науки РФ, 6 заслуженных работников высшей школы РФ, 2 заслуженных учителя РФ, 18 членов общественных академий.

Свыше 14 500 студентов, в том числе почти 770 студентов из 23 зарубежных стран, осваивают программы 53 направлений бакалавриата (136 профилей), 30 направлений магистратуры (80 направленностей), 2 специальностей, 14 направлений аспирантуры (43 направленности). Каждый из студентов имеет возможность вести научно-исследовательскую работу под руководством ученых, имеющих международную известность.

Иркутский государственный университет выпустил более 115 000 человек, в их числе видные ученые, педагоги, писатели, общественные деятели, имеющие международную известность.''']
db = Data_base
class Actions:
    status = 0
    subject = 0
    group = 0
    full_name = 0
    @staticmethod
    def enter_system():
        global status
        global login
        check_enter = False
        while check_enter == False:
            login = input('Введите логин: ')
            password = input('Введите пароль: ')
            for i in db.login_passwords:
                if check_enter == True:
                    break
                if login == i[0]:
                    if password  == i[1]:
                        print('Авторизация прошла успешно')  
                        check_enter = True
                        break
            if login != i[0] or password  != i[1]:
                print('Вы ввели неверный логин или пароль')
                    
        if check_enter == True:
            for i in db.status_:
                if login == i[0]:
                    print('Вы вошли как ',i[1])
                    global status
                    status = i[1]
                
    @staticmethod 
    def show_literature_teacher():
        sibject = show_information.subject
        if subject == 'Математика':
            for i in db.math_list:
                print(i)
        if subject == 'История':
            for i in db.history_list:
                print(i)
        if subject == 'Программирование':
            for i in db.programming_list:
                print(i)
        if subject == 'Русский язык':
            print('Актуальный список литературы отсутсвует')
    @staticmethod 
    def show_literature_student():
        subject_ = input('Литература по предмету\n:')
        if subject_ == 'Математика':
            for i in db.math_list:
                print(i)
        if subject_ == 'История':
            for i in db.history_list:
                print(i)
        if subject_ == 'Программирование':
            for i in db.programming_list:
                print(i)
        if subject_ == 'Русский язык':
            print('Актуальный список литературы отсутсвует')
    @staticmethod 
    def show_information():
        global subject
        global group
        global full_name
        subject = 0
        if status == 'студент':
            for i in db.students__:
                lst = i[0].split()
                surname = lst[0]
                if surname == login:
                    full_name = i[0]
                    course = i[1]
                    group = i[2]
                    print(i[0])
                    print(course, ' курс')
                    print('группа ',group)
        if status == 'преподаватель':
            for i in db.teachers:
                lst = i[0].split()
                surname = lst[0]
                if surname == login:
                    full_name = i[0]
                    subject = i[1]
                    print(i[0])
                    print(i[1])
    @staticmethod
    def new_student():
        students_name = input('Добавление нового студента\nВведите ФИО: ')
        students_course = input('Введите курс: ')
        students_group = input('Введите группу: ')
        new_student = [students_name,students_course,students_group]
        db.students__.append(new_student)
        db.status_.append((students_name, 'студент'))
        db.login_passwords.append(students_name, '0000')
    @staticmethod
    def del_student():
        students_name = input('Удаление студента из базы данных\nВведите ФИО: ')
        for i in db.students:
            if i[0] == students_name:
                db.students.remove(students_name)
                break
        for i in db.login_passwords:
            if i[0] == students_name:
                db.login_passwords(students_name)
                
    @staticmethod
    def show_students():
        for i in db.students__:
            print(i[0],i[1],i[2])
    @staticmethod
    def show_teachers():
        for i in db.teachers:
            print(i[0],i[1])
    @staticmethod
    def show_information_about_university():
        for i in db.information_about_university:
            print(i)
    @staticmethod
    def change_points():
        
        if subject == 'Математика':
            change = input('Выберите группу\n1 - 15567-DH\n2-17767-DH\n:')
            if change == '1':
                check_change = False
                while check_change == False:
                    if check_change == True:
                        break                    
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '15567-DH' and change2==lst[0]:
                            print(lst[0])
                            print('Баллов на данный момент  --  ', db.academic_performance_math_15567_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_math_15567_DH.update({lst[0]: change3})
                            check_change = True
                            break
            if change == '2':
                check_change = False
                while check_change == False:
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    if check_change == True:
                        break
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '17767-DH' and change2==lst[0]:
                            print('Баллов на данный момент  --  ', db.academic_performance_math_17767_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_math_17767_DH.update({lst[0]: change3})
                            check_change = True
                            break
        if subject == 'История':
            change = input('Выберите группу\n1 - 15567-DH\n2-17767-DH\n:')
            if change == '1':
                check_change = False
                while check_change == False:
                    if check_change == True:
                        break                    
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '15567-DH' and change2==lst[0]:
                            print(lst[0])
                            print('Баллов на данный момент  --  ', db.academic_performance_history_15567_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_history_15567_DH.update({lst[0]: change3})
                            check_change = True
                            break
            if change == '2':
                check_change = False
                while check_change == False:
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    if check_change == True:
                        break
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '17767-DH' and change2==lst[0]:
                            print('Баллов на данный момент  --  ', db.academic_performance_history_17767_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_history_17767_DH.update({lst[0]: change3})
                            check_change = True
                            break
        if subject == 'Программирование':
            change = input('Выберите группу\n1 - 15567-DH\n2 - 17767-DH\n:')
            if change == '1':
                check_change = False
                while check_change == False:
                    if check_change == True:
                        break                    
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '15567-DH' and change2==lst[0]:
                            print(lst[0])
                            print('Баллов на данный момент  --  ', db.academic_performance_programming_15567_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_programming_15567_DH.update({lst[0]: change3})
                            check_change = True
                            break
            if change == '2':
                check_change = False
                while check_change == False:
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    if check_change == True:
                        break
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '17767-DH' and change2==lst[0]:
                            print('Баллов на данный момент  --  ', db.academic_performance_programming_17767_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_programming_17767_DH.update({lst[0]: change3})
                            check_change = True
                            break
        if subject == 'Русский язык':
            change = input('Выберите группу\n1 - 15567-DH\n2-17767-DH\n:')
            if change == '1':
                check_change = False
                while check_change == False:
                    if check_change == True:
                        break                    
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '15567-DH' and change2==lst[0]:
                            print(lst[0])
                            print('Баллов на данный момент  --  ', db.academic_performance_programming_15567_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_rus_15567_DH.update({lst[0]: change3})
                            check_change = True
                            break
            if change == '2':
                check_change = False
                while check_change == False:
                    change2 = input('кому изменить баллы\nВведите фамилию\n:')
                    if check_change == True:
                        break
                    for i in db.students__:
                        lst = i[0].split()
                        if i[2] == '17767-DH' and change2==lst[0]:
                            print('Баллов на данный момент  --  ', db.academic_performance_programming_17767_DH[lst[0]])
                            change3 = input('Изменить на :')
                            db.academic_performance_rus_17767_DH.update({lst[0]: change3})
                            check_change = True
                            break
    @staticmethod
    def show_academic_perfomance():
        if status =='преподаватель':
            if subject == 'Математика':
                print('15567-DH')
                for i in db.academic_performance_math_15567_DH:
                    print(i + '  --  ',db.academic_performance_math_15567_DH[i])
                print('17767-DH')
                for i in db.academic_performance_math_17767_DH:
                    print(i + '  --  ',db.academic_performance_math_17767_DH[i])
            if subject == 'История':
                print('15567-DH')
                for i in db.academic_performance_math_15567_DH:
                    print(i + '  --  ',db.academic_performance_history_15567_DH[i])
                print('17767-DH')    
                for i in db.academic_performance_math_17767_DH:
                    print(i + '  --  ',db.academic_performance_history_17767_DH[i])
            if subject == 'Русский язык':
                print('15567-DH')
                for i in db.academic_performance_math_15567_DH:
                    print(i + '  --  ',db.academic_performance_rus_15567_DH[i])
                print('17767-DH')
                for i in db.academic_performance_math_17767_DH:
                    print(i + '  --  ',db.academic_performance_rus_17767_DH[i])
            if subject == 'Программирование':
                print('15567-DH')
                for i in db.academic_performance_math_15567_DH:
                    print(i + '  --  ',db.academic_performance_programming_15567_DH[i])
                print('17767-DH')
                for i in db.academic_performance_math_17767_DH:
                    print(i + '  --  ',db.academic_performance_programming_17767_DH[i])
        if status == 'студент':
            if group == '15567-DH':
                print('Математика', db.academic_performance_math_15567_DH[login])
                print('История',db.academic_performance_history_15567_DH[login])
                print('Программирование',db.academic_performance_programming_15567_DH[login])
                print('Русский язык',db.academic_performance_rus_15567_DH[login])
            if group == '17767-DH':
                print('Математика', db.academic_performance_math_17767_DH[login])
                print('История',db.academic_performance_history_17767_DH[login])
                print('Программирование',db.academic_performance_programming_17767_DH[login])
                print('Русский язык',db.academic_performance_rus_17767_DH[login])
    @staticmethod
    def show_timetable():
        if status =='преподаватель':
            print('15567-DH')
            for i in db.timetable_15567_DH:
                print(i[0])
                for g in i[1::]:
                    print(g)
            print('17767-DH')
            for i in db.timetable_17767_DH:
                print(i[0])
                for g in i[1::]:
                    print(g)
        if status == 'студент':
            if group == '15567-DH':
                for i in db.timetable_15567_DH:
                    print(i[0])
                    for g in i[1::]:
                        print(g)                
            if group == '17767-DH':
                for i in db.timetable_17767_DH:
                    print(i[0])
                    for g in i[1::]:
                        print(g)    
    def show_information_about_university():
        for i in db.information_about_university:
            print(i)
class Teacher_menu(Actions):
    def __init__(self,check = 1):
        while True:
            action = input('1 - просмотр списка студентов\n2 - просмотр рассписания\n3 - Добавить нового студента\n4 - Удалить студента\n5 - Просмотр успеваемости\n6 - Поставить баллы\n7 - Просмотр списка литературы\n8 - Просмотр информации о ВУЗe \n9 - войти под другой учетной записью\n:')
            if action == '1':
                Actions.show_students()
            if action == '2':
                Actions.show_timetable()
            if action == '3':
                Actions.new_student()
            if action == '4':
                Actions.del_student()
            if action == '5':
                Actions.show_academic_perfomance()
            if action == '6':
                Actions.change_points()
            if action == '7':
                Actions.show_literature_teacher()
            if action == '8':
                Actions.show_information_about_university()
            if action == '9':
                g = Actions()
                j = System(g)
class Student_menu(Actions):
    def __init__(self, check=1):
        while True:
            action = input('\n1 - список преподавателей\n2 - Просмотр рассписания\n3-Моя успеваемость\n4-Список литературы\n5-Информация о ВУЗе\n6 - войти под другой учетной записью\n:')
            if action == '1':
                Actions.show_teachers()
            if action == '2':
                Actions.show_timetable()
            if action == '3':
                Actions.show_academic_perfomance()
            if action == '4':
                Actions.show_literature_student()
            if action == '5':
                Actions.show_information_about_university()
            if action == '6':
                g = Actions()
                j = System(g)
        
class System(Actions):
    def __init__(self, check = 1):
        self.status = Actions.status
        Actions.enter_system()
        Actions.show_information()
        if status == 'студент':
            c = Student_menu(a) 
        if status == 'преподаватель':
            c = Teacher_menu(a)
    
    
        
a = Actions()
b = System(a)
    


    
    

    
